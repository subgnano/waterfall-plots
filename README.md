# Waterfall plot

A simple library to help with waterfall plots in python with Matplotlib.

The `How to use.ipynb` show four usage examples. In all cases we plot `z = F(x, y)`, where `x` is along the x-axis, while different values of `y` are shifted to create the 3D illusion.

1. A simple B&W plot without y-axis.
2. A B&W plot with y-axis labels indicating the values of y for each line.
3. A colorful plot with y-labels as in (2), and a color code for the intensity of F (z-axis).
4. A colorful plot where the colors represent the values of y for each line. Using a color map to represent the values of y.
5. A colorful plot where the colors represent the values of y for each line. Using a legend to represent the values of y.

![Waterfall plot](waterfall.png "Waterfall plot")

## How to install

The code is only available here at the gitlab repository, to install it, run the line below. Notice that the `--upgrade` option is only necessary for upgrades.

```bash
pip install [--upgrade] git+https://gitlab.com/subgnano/waterfall-plots.git
```

## Credits

Everything here is based (copy-paste + improvements) on the examples and discussions from:

- [StackOverflow Question 55781132](https://stackoverflow.com/questions/55781132/imitating-the-waterfall-plots-in-origin-with-matplotlib)
- [ljbkusters github repos](https://github.com/ljbkusters/mpl-waterfall-plot)
- The suggestion and initial version of case 5 above was developed by Olavo Viola (INFIS/UFU).
