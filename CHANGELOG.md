# Changelog

## version 0.1.2

- New feature: option to plot y values as legends. Suggestion and initial version developed by Olavo Viola (INFIS/UFU).

## version 0.1.1

- Option to define limits of colorbars (cmin, cmax)
- Fmap and Ymap plots return cbardata to be used to generate and manipulate colorbars externally

## version 0.1.0

- First version, needs to be tested before reaching the 1.0.0 status
- Four types of waterfall plots: simple B&W, with y-labels, cmap as intensity, cmap as y
