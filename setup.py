from setuptools import setup, find_packages

python_version = '>=3.9'
install_requires = ['numpy', 'matplotlib']

# uses README.md as the package long description
with open("README.md") as f:
    long_description = f.read()

# read version from module
exec(open('waterfall/__version.py').read())

setup(
    name="Waterfall Plots",
    description="Routines to make waterfall plots",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/subgnano/waterfall-plots",
    author="Gerson J. Ferreira",
    author_email="gersonjferreira@ufu.br",
    version=__version__,
    packages=find_packages('.'),
    install_requires=install_requires,
    python_requires=python_version)
